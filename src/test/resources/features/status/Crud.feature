# new feature
# Tags: optional

Feature:  CRUD Operations
Scenario: Add Employee record
Given I Set POST employee service api endpoint
When I Set request HEADER
And Send a POST HTTP request
Then I receive valid Response

  Scenario: Add Employee record
    Given I Set PUT employee service api endpoint
    When I Set update request body
    And Send a PUT HTTP request
    Then I receive valid HTTP Response with 200

  Scenario: Add Employee record
    Given I Set GET employee service api endpoint
    When I Set request HEADER
    And Send a GET HTTP request
    Then I receive valid HTTP Response with 200

  Scenario: Add Employee record
    Given I Set DELETE employee service api endpoint
    When I Set request HEADER
    And Send a Delete HTTP request
    Then I receive valid Response with 200