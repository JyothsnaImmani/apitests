package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import starter.status.Queryparams;


public class query {

    @Steps
    Queryparams query1;

    @Given("I enter url with query params and check the response")
    public void iEnterUrlWithQueryParamsAndCheckTheResponse()
    {
        query1.M1();

    }

    @Then("I post the request")
    public void iPostTheRequest() {
        query1.M2();
    }
}
