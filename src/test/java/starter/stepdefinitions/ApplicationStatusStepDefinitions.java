package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.status.ApplicationStatus;


public class ApplicationStatusStepDefinitions {

    @Steps
    ApplicationStatus theApplication;

    @Given("I enter url and postcode: and check the status")
    public void the_application_is_running() {
        theApplication.currentStatus();

    }

    @Then("I check statusline and content")
    public void iCheckStatuslineAndContent()
    {
        theApplication.getstatus();
    }
}
