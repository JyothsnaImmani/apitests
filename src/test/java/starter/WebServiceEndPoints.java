package starter;

public enum WebServiceEndPoints {

    STATUS("http://dummy.restapiexample.com");

    private final String url;

    WebServiceEndPoints(String url)
    {
        this.url = url;
    }

    public String getUrl()

    {
        return url;
    }
}
