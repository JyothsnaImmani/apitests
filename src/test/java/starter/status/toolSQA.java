package starter.status;


import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.jruby.RubyProcess;
import org.junit.Assert;
import org.yecht.Data;

public class toolSQA {

    private static final String BASE_URL = "https://bookstore.toolsqa.com";
    private static final String USER_ID = "9b5f49ab-eea9-45f4-9d66-bcf56a531b85";
    private static final String USERNAME = "TOOLSQA-Test";
    private static final String PASSWORD = "Test@@123";


    @Step
    public void Method1()
    {
    RestAssured.baseURI = BASE_URL;
    RequestSpecification request = RestAssured.given();
    request.header("Content-Type", "application/json");
    Response response = request.get(BASE_URL);
    int Status = response.getStatusCode();
    Assert.assertEquals(Status,200);
    ResponseBody body = response.getBody();
        //String BodyAsString = body.asString();
        System.out.println("body:" +body);

        String contentType = response.header("Content-Type");
        System.out.println("Content-Type value: " + contentType);

        // Reader header of a give name. In this line we will get
        // Header named Server
        String serverType =  response.header("Server");
        System.out.println("Server value: " + serverType);

        // Reader header of a give name. In this line we will get
        // Header named Content-Encoding
        String acceptLanguage = response.header("Content-Encoding");
        System.out.println("Content-Encoding: " + acceptLanguage);

}}
