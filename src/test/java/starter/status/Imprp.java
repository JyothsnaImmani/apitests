package starter.status;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.jruby.RubyProcess;
import org.junit.Test;

public class Imprp {

    @Test
    public void defence()
    {
        RestAssured.baseURI = "https://gdk.improbable.io/l/169082/2020-06-16/2kfkfz";
        RequestSpecification Request = RestAssured.given();
        Response response = Request.get(RestAssured.baseURI);
        int code = response.getStatusCode();
        System.out.println(code);
        System.out.println(response.getBody());
    }
}
