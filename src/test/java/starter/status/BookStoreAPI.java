package starter.status;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import io.vavr.collection.Map;
import org.codehaus.groovy.util.ListHashMap;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.List;

public class BookStoreAPI {

    /*
    @Test
    public void BookStore()
    {

      //  RestAssured.baseURI = "http://bookstore.toolsqa.com";
        RequestSpecification Request = RestAssured.given();
        Request.header("Content-Type","application/json");

        JSONObject json =  new JSONObject();
        json.put("userName","Sahasra");
        json.put("password","Tangocool@12");
        Request.body(json.toJSONString());
        Response response = Request.post("https://bookstore.toolsqa.com/Account/v1/User");
        System.out.println(response.getStatusCode());

        String userId = response.getBody().jsonPath().getString("userId");
        //System.out.println(response.getBody());
        System.out.println(response.getBody().asString());
        // userID generated -  45c83643-0d87-4160-8551-ac9c0ff42320

    }

    @Test
    public void Token()
    {
        RequestSpecification Request = RestAssured.given();
        Request.header("Content-Type","application/json");
        JSONObject json1 = new JSONObject();
        json1.put("userName","Sahasra");
        json1.put("password","Tangocool@12");
        Request.body(json1.toJSONString());
        Response response = Request.post("https://bookstore.toolsqa.com/Account/v1/GenerateToken");

        String token = response.body().jsonPath().getString("token");
        System.out.println(response.getBody().asString());

        // token generated =
        eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6IlNhaGFzcmEiLCJwYXNzd29yZCI6IlRhbmdvY29vbEAxMiIsImlhdCI6MTU5NTc3MjM0M30.kdMnkwY9R9BBPTog2rMfW1PSYW_gTX_hTaXMZt54Go0


    } */

    @Test
    public void Getbooks()
    {
        RestAssured.baseURI = "https://bookstore.toolsqa.com/BookStore/v1/Books";
        RequestSpecification Request = RestAssured.given();

        Response response = Request.get(RestAssured.baseURI);
       System.out.println(response.getBody().asString());
        //System.out.println(response.getStatusCode());
        //response.getBody().asString();
        String body = response.getBody().asString();

       // List<Map<String,String>> books = JsonPath.from(body).getList("books");
        JsonPath JsonPathEvalutor = response.jsonPath();
        String isbn = JsonPathEvalutor.get("isbn");
        System.out.println("Book id:" +isbn);

       // Assert.assertTrue(books.size() > 0);

         // books.get(0).get("isbn");

       // System.out.println(response.getStatusCode());
       // System.out.println(response.getBody().asString());



    }

}
