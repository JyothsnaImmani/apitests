package starter.status;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.jruby.RubyProcess;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class put {

    @Test
    public void M2()
    {
        RequestSpecification Request = RestAssured.given();
        Request.header("Content-Type","application/json");
        JSONObject json = new JSONObject();
        json.put("id","4");
        json.put("title","updated");
        json.put("author","Sahasra");
        Request.body(json.toJSONString());
        Response response = Request.put("http://localhost:3000/posts/4");
        int code = response.getStatusCode();
        System.out.println(response.getStatusCode());
        System.out.println(response.getBody());
        Assert.assertEquals(code,200);

    }


}
