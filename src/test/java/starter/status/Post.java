package starter.status;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class Post {
    @Test
    public void test1()
    {
        //RestAssured.baseURI = "http://localhost:3000/posts";
        RequestSpecification Request = RestAssured.given();
        Request.header("Content-Type","application/json");

        JSONObject json = new JSONObject();
        json.put("id","12");
        json.put("title","tandav2");
        json.put("author","Jyoth2");
        Request.body(json.toJSONString());
        Response response = Request.post("http://localhost:3000/posts");
        System.out.println(response.getBody());
        int code = response.getStatusCode();
                System.out.println(code);
        System.out.println(response.statusLine());
        Assert.assertEquals(code,201);
    }


}
