package starter.status;

import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;
import org.jruby.RubyProcess;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class Queryparams {

    private static String baseurl = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1";
    private static String url = "https://restapi.demoqa.com/customer";

    @Step
    public void M1() {

        RestAssured.baseURI = baseurl;
        RequestSpecification Request = RestAssured.given();
        //Response response = Request.queryParam("zip: | 94040,us").queryParam("appid: | 439d4b804bc8187953eb36d2a8c26a02").get("weather");
        //String JsonString = response.asString();
        //System.out.println("Status code:" + response.getStatusCode());
        Response response = Request.get(baseurl);
        //System.out.println("Body is" + response.getBody());
        //Assert.assertEquals(JsonString.contains("London"), true);

        ResponseBody Body = response.getBody();
        String bodyAsString = Body.asString();
        System.out.println("Body is" + bodyAsString + "Time taken:" +response.getTimeIn(TimeUnit.MILLISECONDS));
        System.out.println("Test Passed");
    }

    @Step
    public void M2() {
        RestAssured.baseURI = url;
        RequestSpecification Request = RestAssured.given();
        System.out.println("Url Hit Passed");
        JsonObject RequestParams = new JsonObject();
        System.out.println("Obj created");
        RequestParams.addProperty("FirstName", "Jyosthsna");
        RequestParams.addProperty("Lastname", "Immani");
        RequestParams.addProperty("Username", "Jyo11");
        RequestParams.addProperty("Password", "Password124");
        RequestParams.addProperty("Email", "immanijyothsna@gmail.com");
        System.out.println("Values are entered");

        Request.body(RequestParams.toString());
        Response response = Request.post("/register");
        System.out.println("Post is sucessful");

        int StatusCode = response.statusCode();
       // Assert.assertEquals("StatusCode","201" );
        String sucessCode = response.jsonPath().get("sucessCode");
        //Assert.assertEquals("correct status code",sucessCode,"OPERATIONSuccess");

        String StatusLine = response.statusLine();
        System.out.println("Status line is:" +StatusLine);


    }
}