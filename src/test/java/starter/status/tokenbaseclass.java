package starter.status;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.BeforeClass;

public class tokenbaseclass {

    @BeforeClass
    public static void base()
    {
        RestAssured.authentication = RestAssured.preemptive().basic("ToolsQA","TestPassword") ;
    }
}
