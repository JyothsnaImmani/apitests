package starter.status;

import com.google.common.base.Preconditions;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.Ensure;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.rest.decorators.request.RequestSpecificationDecorated;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.jruby.RubyProcess;
import org.junit.Assert;
import org.junit.Test;
import starter.WebServiceEndPoints;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.is;
import static starter.WebServiceEndPoints.STATUS;

public class ApplicationStatus {
    private static final String baseurl = "http://api.postcodes.io/postcodes/SW1P4JA";


    @Step
    public void currentStatus() {
        RestAssured.baseURI = "baseurl";
        RequestSpecification Request = RestAssured.given();
        //Response response = Request.get("/SW1P4JA");
        Response response = Request.get(baseurl);
        int StatusCode = response.statusCode();
        Assert.assertEquals(StatusCode, 200);

        ResponseBody body = response.getBody();
        String bodyAsString = body.asString();
        System.out.println(bodyAsString);
    }
    @Step
    public void getstatus()
    {

        RestAssured.baseURI = "baseurl";
        RequestSpecification request = RestAssured.given();
        Response response = request.get(baseurl);
        String statsuline = response.statusLine();

        Assert.assertEquals("HTTP/1.1 200 OK","HTTP/1.1 200 OK" );
        System.out.println("StatusLine:" +statsuline);

        String ContentType = response.getContentType();
        Assert.assertEquals("Content Type","Content Type");
        System.out.println("Content Type:" +ContentType);

        String ServerType = response.header("ServerType");
        System.out.println("ServerType:" +ServerType);

        String contentEncoding = response.header("contentEncoding");
        Assert.assertEquals("application/json; charset=utf-8","application/json; charset=utf-8");
        System.out.println("contentEncoding:" +contentEncoding );

        JsonPath jsonPathEvaluator = response.jsonPath();

        String country = jsonPathEvaluator.get("country");
        System.out.println("country name:" +country);

        System.out.println("longitude is" + jsonPathEvaluator.get("longitude"));
        System.out.println("constituency is" + jsonPathEvaluator.get("parliamentary_constituency"));
      Assert.assertEquals("parliamentary_constituency","parliamentary_constituency");

    }

    }

