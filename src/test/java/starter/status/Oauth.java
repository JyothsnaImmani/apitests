package starter.status;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.jruby.RubyProcess;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class Oauth {

    /*@Test
    public void tes1()
    {
        //RequestSpecification Request = RestAssured.given();
        //Request.header("Content-Type","application/json");
        //JSONObject Json = new JSONObject();
        //Json.
        // check response wih access token provided
        Response Response = RestAssured.
                given().
                auth().
                oauth2("beed002402c4e70adbc3dfaf563d985a3f4a39c3").
                post("http://coop.apps.symfonycasts.com/api/1225/chickens-feed");


        System.out.println(Response.getStatusCode());
        System.out.println(Response.getBody().asString());

    }
*/
    @Test
    public void AccessvalidAPI()
    {
        //Authorize genrate token and get the response
        Response Response = RestAssured.
                given().
                formParam("client_id","JyoTest1").
                formParam("client_secret","0b9d2129051c63426a48b9148cde419f").
                formParam("grant_type","client_credentials").
                post("http://coop.apps.symfonycasts.com/token");

        System.out.println(Response.getBody().asString());
        String token = Response.jsonPath().get("access_token");
        System.out.println("token is :" +token);
        //{"access_token":"9d4fcd6abf31de4b365ba85aaa037f4502f7f06f","expires_in":86400,"token_type":"Bearer","scope":"chickens-feed"}

        Response Response1 = RestAssured.
                given().
                auth().
                oauth2(token).
                post("http://coop.apps.symfonycasts.com/api/1225/chickens-feed");
        System.out.println(Response1.getBody().asString());

        Assert.assertEquals(Response1.getStatusCode(),200); }

    @Test
    public void invalidApi()
    {
    Response response = RestAssured.given()
            .formParam("client_id","JyoTest1")
            .formParam("client_secret","0b9d2129051c63426a48b9148cde419f")
            .formParam("grant_type","client_credentials")
            .post("http://coop.apps.symfonycasts.com/token");
    System.out.println(response.getBody().asString());
    String token1 = response.jsonPath().get("access_token");


    Response Response1 = RestAssured.given()
            .auth()
            .oauth2(token1)
            .post("http://coop.apps.symfonycasts.com/api/1225/eggs-collect");
    System.out.println(Response1.getBody().asString());
    Assert.assertEquals(Response1.getStatusCode(),200);
}
}
